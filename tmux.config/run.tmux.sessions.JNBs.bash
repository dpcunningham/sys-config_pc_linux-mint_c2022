#!/bin/bash

# Useful Targeting Spec: tmux attach -t dev_JNBs:dax_school.1	# ie: tmux a -t session:window.pane

# Additional Useful Examples:
# 1) Unix Stackexchange: https://unix.stackexchange.com/a/554244
# 2) Superuser Stack Exchange: https://superuser.com/a/492549 

# --------------------------------------
# Window/Panes for DevOps & Downloads:
# --------------------------------------
tmux new -d -s dev_JNBs -n devops -c ~/Downloads

tmux send-keys -t dev_JNBs:devops "top" Enter
tmux split-window -h -t dev_JNBs:devops
tmux send-keys -t dev_JNBs:devops "neofetch" Enter
tmux split-window -v -t dev_JNBs:devops -c ~/Downloads
tmux send-keys -t dev_JNBs:devops "cowsay \"Whatcha got in ~/Downloads, Bruh?\"" Enter

# ---------------------------------------------------------------------------------------------------------------
# Project: dax_school:
work_dir=~/dpc.data/local.FS/lfs.02-Work.in.Progress/explore-webdata-ETL/using-python/scraping-w--bs/dax_school-data/
notebook=./daq_school-data_active-feature-dev.ipynb 
# ---------------------------------------------------------------------------------------------------------------
tmux new-window -n dax_school -t dev_JNBs: -c "$work_dir"
tmux send-keys -t dev_JNBs:dax_school.1 "source ./project-venv/bin/activate && sleep 1" Enter
tmux send-keys -t dev_JNBs:dax_school.1 "jupyter notebook ${notebook} &" 
tmux split-window -v -p66 -t dev_JNBs:dax_school -c "$work_dir"
tmux send-keys -t dev_JNBs:dax_school.2 "ls" Enter


# ---------------------------------------------------------------------------------------------------------------
# Project: daq_schwab:
work_dir=~/dpc.data/local.FS/lfs.03-Projects.Active/HNWtools/dax_data-acquisition-systems/dax-selenium/daq_tda-selenium/
notebook=./daq_schwab-anti-bot_vs_stealth-selenium-ucd.ipynb 
# ---------------------------------------------------------------------------------------------------------------
tmux new-window -n daq_schwab -t dev_JNBs: -c "$work_dir"
tmux send-keys -t dev_JNBs:daq_schwab.1 "pwd" Enter Enter "ls" Enter
tmux split-window -v -p33 -t dev_JNBs:daq_schwab -c "$work_dir"
tmux send-keys -t dev_JNBs:daq_schwab.2 "source ./project-venv/bin/activate && sleep 1" Enter "jupyter notebook ${notebook} &" 


# ---------------------------------------------------------------------------------------------------------------
# Project: daq_yf_utils:
work_dir=~/dpc.data/local.FS/lfs.03-Projects.Active/develop.trading.systems/dax_data-acquisition-systems/daq_yfinance/
notebook=./daq_yf_utils.ipynb 
# ---------------------------------------------------------------------------------------------------------------
tmux new-window -n daq_yfinance -t dev_JNBs: -c "$work_dir"
tmux send-keys -t dev_JNBs:daq_yfinance.1 "pwd" Enter Enter "ls" Enter
tmux split-window -v -p33 -t dev_JNBs:daq_yfinance -c "$work_dir"
tmux send-keys -t dev_JNBs:daq_yfinance.2 "source ./project-venv/bin/activate && sleep 1" Enter "jupyter notebook ${notebook} &"


# ------------------------------------------------------
# Finally, attach to our current window-of-interest
# ------------------------------------------------------
tmux attach -t dev_JNBs:dax_school.1 		# per: tmux a -t session:window.pane

